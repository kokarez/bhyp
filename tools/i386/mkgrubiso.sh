#!/bin/sh

set -ue

usage_die ()
{
  progname="$(basename "$0")"
  echo "usage: $progname -o output.iso -s stage2_eltorito -m menu.lst [ -l label ] [ source:target ]..."
  exit 1
}

while getopts o:s:m:l: arg; do
  case "$arg" in
    o) output="$OPTARG";;
    s) stage2="$OPTARG";;
    m) menulst="$OPTARG";;
    l) label="$OPTARG";;
    ?) usage_die;;
  esac
done

if [ -z "$output" -o -z "$stage2" -o -z "$menulst" ]; then
  usage_die
fi

rootfs="$(mktemp -d)"
mkdir -p "$rootfs/boot/grub"

cp "$stage2" "$rootfs/boot/grub/stage2_eltorito"
cp "$menulst" "$rootfs/boot/grub/menu.lst"

shift $(($OPTIND - 1))

while [ $# -ne 0 ]; do
  source="${1%%:*}"
  target="${1##*:}"
  cp -r "$source" "$rootfs/$target"
  shift
done

mkisofs -quiet -R -b "boot/grub/stage2_eltorito" -no-emul-boot -boot-load-size 4 -boot-info-table -A "${label:-$output}" -o "$output" "$rootfs"

rm -rf "$rootfs"
