#ifndef INTERRUPT_H_
# define INTERRUPT_H_

# include <arch/arch/regs.h>

enum interrupt_flag{
  TRAP_FLAG = 1,
  USER_FLAG = 2,
  WRAPPED_FLAG = 4
};

typedef void (*interrupt_handler)(struct info_registers *regs);

void int_register(interrupt_handler handler, uint number, enum interrupt_flag flags);

#endif /* !INTERRUPT_H_ */
