/* see: http://git.lse.epita.fr/?p=six.git */

#ifndef PERVASIVE_H_
# define PERVASIVE_H_

/*
** This file is automatically included in each C file. It defines new types,
** which should be more meaningful than standard C files.
** It is called pervasive.h as a reference to OCaml's pervasive module which is
** automatically included in each compiled file.
*/

/* Standard processor word. */
typedef   long int              word;
typedef   unsigned long int     uword;

/* This eases the use of unsigned types with a shorter name. */
typedef   unsigned char         byte;
typedef   unsigned int          uint;

/* Boolean manipulation. */
typedef   _Bool                 bool;
# define  true                  1
# define  false                 0

/*
** Sized types, should only used when size really matters.
** ANSI-C only specifies minimum sizes of each integer type so we rely on the
** compiler for the correctness of these typedefs.
*/
typedef   unsigned char           uint8;
typedef   char                    sint8;
typedef   unsigned short int      uint16;
typedef   short int               sint16;
typedef   unsigned int            uint32;
typedef   int                     sint32;
typedef   unsigned long long int  uint64;
typedef   long long int           sint64;

/* Manipulate a pointer as an integer. */
# if __SIZEOF_POINTER__ == 4
typedef   uint32                  uintptr;
# elif __SIZEOF_POINTER__ == 8
typedef   uint64                  uintptr;
# else
#  error unknown pointer size
# endif

/* Another useful one. */
# define  NULL                    ((void*) 0)

/*
** Data type limits.
** There may be a better way to define them.
*/
# define  CHAR_MIN                (-128)
# define  CHAR_MAX                (127)
# define  BYTE_MIN                (0)
# define  BYTE_MAX                (255)
# define  INT_MIN                 (-2147483648)
# define  INT_MAX                 (2147483647)
# define  UINT_MIN                (0)
# define  UINT_MAX                (4294967296)

#endif /* !PERVASIVE_H_ */
