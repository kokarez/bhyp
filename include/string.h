#ifndef STRING_H_
# define STRING_H_

void  *memcpy(void *dst, const void *src, uint n);
void  *memset(void *s, int b, uint n);
uint strlen(const char *s);

#endif /* !STRING_H_ */
