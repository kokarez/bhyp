#ifndef STDLIB_H_
# define STDLIB_H_

extern uint _stack_start;

void *kalloc(uint size);
void kfree(void *ptr);

#endif /* !STDLIB_H_ */
