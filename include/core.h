#ifndef CORE_H_
# define CORE_H_

# define halt() __halt()

/* Method to print in one line of 80 chars */
void print_ok(const char *str);
void print_ko(const char *str);
void print_center(const char *str);

/* Basic methods to print */
void kprint(const char *str, uint len);
void panic(const char *str);

#endif /* !CORE_H_ */
