#ifndef REGS_H_
# define REGS_H_

struct info_registers
{
  uint32 ebx;
  uint32 ecx;
  uint32 edx;
  uint32 esi;
  uint32 edi;
  uint32 ebp;
  uint32 ds;
  uint32 eax;
  uint32 int_num;
  uint32 err_num;
}__attribute__((packed));

#endif /* !REGS_H_ */
