#ifndef MM_H_
# define MM_H_

# include <external/multiboot.h>

/*
 * Pages of 4Ko
 */

# define MM_PG_SIZE	4096
# define PD_BASE	(0x20000)		// FIXME: arbitrary
# define PD_SIZE	4096			// 32 bits mode
# define PT_SIZE	4096			// 32 bits mode
# define MM_KMMAP_PTE	(PD_BASE + PD_SIZE * 2)	// pte for kmmap section
//# define MM_KMMAP	mmap_addr		// begining of kmmap section (phy)
# define MM_KMMAP	(PD_BASE * 2)		// begining of kmmap section (phy)
# define MM_KMMAP_VM	(MM_KMMAP_PTE + PT_SIZE + (1 << 21))
# define MM_KMMAP_SIZE	(PD_SIZE / 4)		// 1 pde for kmmap,


/*
 * Kernel land memory is as follow:
 *
 * |			|
 * |			|
 * |   kmem (by kmmap)	|	MM_KMMAP
 * |			|
 * |   2nd page entry	|	MM_KMMAP_PTE
 * |  (for kernel mmap)	|
 * |   1st page entry	|	PD_BASE + PD_SIZE
 * |   page directory	|	PD_BASE
 * |			|
 * |			|
 * |			|
 * |   kernel stuff	|	0x00000
 * +____________________+
 *
 */


typedef struct
{
	uint32		ignored		: 3;
	uint32		pwt		: 1;
	uint32		pcd		: 1;
	uint32		ignored2	: 7;
	uint32		pde_addr	: 20;
} __attribute__ ((packed)) mm_cr3_t;


typedef struct
{
	uint32		p		: 1;	// present
	uint32		rw		: 1;
	uint32		us		: 1;	// User/supervisor
	uint32		pwt		: 1;	// Page-level write-through
	uint32		pcd		: 1;	// Page-level cache disable
	uint32		a		: 1;	// Accessed
	uint32		ignored		: 1;
	uint32		ps		: 1;	// 4MB page if ps = 1
	uint32		ignored2	: 4;
	uint32		pte_addr	: 20;
} __attribute__ ((packed)) mm_pde_t;

typedef struct
{
	uint32		p		: 1;	// present
	uint32		rw		: 1;
	uint32		us		: 1;	// User/supervisor
	uint32		pwt		: 1;	// Page-level write-through
	uint32		pcd		: 1;	// Page-level cache disable
	uint32		a		: 1;	// Accessed
	uint32		d		: 1;	// dirty
	uint32		ps		: 1;	// 4MB page if ps = 1
	uint32		g		: 1;	// Ignored if ps = 0 || CR4.PGE = 0
	uint32		ignored2	: 3;
	uint32		pat		: 1;
	uint32		addr		: 4;
	uint32		reserved	: 5;	// must be 0
	uint32		addr2		: 10;
} __attribute__ ((packed)) mm_pde_4m_t;

typedef struct
{
	uint32		p		: 1;	// present
	uint32		rw		: 1;
	uint32		us		: 1;
	uint32		pwt		: 1;	// Page-level write-through
	uint32		pcd		: 1;	// Page-level cache disable
	uint32		a		: 1;	// Accessed
	uint32		d		: 1;	// Dirty
	uint32		pat		: 1;
	uint32		g		: 1;	// Global
	uint32		ignored		: 3;
	uint32		pf_addr		: 20;	// page frame
} __attribute__ ((packed)) mm_pte_t;


typedef struct
{
        uint8 p       : 1;
        uint8 wr      : 1;
        uint8 us      : 1;
        uint8 rsvd    : 1;
        uint8 id      : 1;
} __attribute__ ((packed)) pf_err_code_t;




void paging_mode_init (multiboot_info_t *info);
void *kmmap(uint size);
int kmunmap(void *addr, uint lenght);

#endif /* !MM_H_ */
