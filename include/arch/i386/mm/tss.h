#ifndef TSS_H_
# define TSS_H_

struct tss {
  uint16 prev_task_link, unused1;
  uint32 esp0;
  uint16 ss0, unused2;
  uint32 esp1;
  uint16 ss1, unused3;
  uint32 esp2;
  uint16 ss2, unused4;
  uint32 cr3, eip, eflags, eax, ecx, edx, ebx, esp, ebp, esi, edi;
  uint16 es, unused5;
  uint16 cs, unused6;
  uint16 ss, unused7;
  uint16 ds, unused8;
  uint16 fs, unused9;
  uint16 gs, unused10;
  uint16 ldt_segment_selector, unused11;
  uint16 t:1, unused12:15;
  uint16 io_map_base_address;
  uint8 idt[32];
  uint8 io[8192];
  uint8 end;
} __attribute__ ((packed));

#endif /* !TSS_H */
