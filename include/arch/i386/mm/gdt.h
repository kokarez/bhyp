#ifndef GDT_H_
# define GDT_H_

# include <stdio.h>
# include <string.h>
# include <arch/i386/asm/crx.h>

# define USER_BASE_DATA 0x300000
# define USER_BASE_CODE 0x200000

# define SELECTOR(Entry, Rpl)	((Entry << 3) + Rpl)

# define KERNEL_CS		1
# define KERNEL_CODE_SELECTOR		SELECTOR(KERNEL_CS, 0)
# define KERNEL_DS		2
# define KERNEL_DATA_SELECTOR		SELECTOR(KERNEL_DS, 0)
# define USER_CS		3
# define USER_CODE_SELECTOR		SELECTOR(USER_CS, 3)
# define USER_DS		4
# define USER_DATA_SELECTOR		SELECTOR(USER_DS, 3)
# define KERNEL_TSS		5
# define KERNEL_TSS_SELECTOR		SELECTOR(KERNEL_TSS, 0)

#define CODE_SEGMENT(Dpl) (((Dpl) << 5) | (1 << 4) | 0xA)
#define DATA_SEGMENT(Dpl) (((Dpl) << 5) | (1 << 4) | 0x3)
#define TSS_SEGMENT(Dlp) (((Dlp) << 5) | 0x9)

#define GDT_MAX_SIZE 128

struct gdte
{
	uint16	limit_0_15;
	uint16	base_0_15;
	uint8	base_16_23;
	uint8	access:7;
	uint8	p:1;
	uint8	limit_16_19:4;
	uint8	avl:1;
	uint8	l:1;
	uint8	granularity:2;
	uint8	base_24_31;
} __attribute__ ((packed));

struct gdtr
{
	uint16	limit;
	uint32	base;
} __attribute__ ((packed));

#  define SEG_REG(Name)					\
	static inline void set_ ## Name (uint16 v)		\
	{						\
		__asm__("movw %0, %%" #Name "\n\t"	\
			: /* No output */		\
			: "r" (v)			\
			: "memory");			\
	}						\
	static inline uint16 get_ ## Name (void)		\
	{						\
		uint16 ret;				\
							\
		__asm__("movw %%" #Name ", %0\n\t"	\
			: "=&r" (ret));			\
		return ret;				\
	}

SEG_REG(ds)
SEG_REG(es)
SEG_REG(fs)
SEG_REG(gs)
SEG_REG(ss)

static inline void set_cs(uint32 v)
{
	__asm__("pushl %0\n\t"
		"pushl $1f\n\t"
		"lret\n\t" /* pop EIP, then %cs */
		"1:\n\t"
		: /* No output */
		: "r" (v)
		: "memory");
}

static inline uint16 get_cs(void)
{
	uint16 ret;

	__asm__("movw %%cs, %0\n\t"
		: "=&r" (ret));
	return ret;
}

static inline void lgdt(struct gdtr gdtr)
{
  __asm__("lgdt %0\n\t"
      :
      : "m" (gdtr)
      : "memory");
}

static inline void ltrw(uint16 tss_sel)
{
  __asm__("ltrw %0\n\t"
      :
      : "r" (tss_sel)
      : );
}

int set_gdt_gate(uint32, uint32, uint8, uint8, int n);

void gdt_init(void);

void set_tss_esp0(uint32);

#endif	/* GDT_H_ */

