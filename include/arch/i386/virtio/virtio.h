#ifndef VIRTIO_H_
# define VIRTIO_H_

# include <vfs/vfs.h>
# include <arch/i386/pci/pci_drivers.h>
# include <arch/i386/asm/io.h>
# include <arch/i386/mm/mmu.h>
# include <arch/i386/virtio/virtio-config.h>

# define VIRTIO_VENDORID 0x1AF4

/* ==> osDev */

# define VIRTIO_DEVICEID_BEGIN 0x1000
# define VIRTIO_DEVICEID_END   0x103F

# define VIRTIO_PCI_DEVICE_FEATURE  0x00
# define VIRTIO_PCI_GUEST_FEATURE   0x04
# define VIRTIO_PCI_QUEUE_ADDR      0x08
# define VIRTIO_PCI_QUEUE_SIZE      0x0C
# define VIRTIO_PCI_QUEUE_SEL       0x0E
# define VIRTIO_PCI_QUEUE_NOTIFY    0x10
# define VIRTIO_PCI_DEVICE_STATUS   0x12
# define VIRTIO_PCI_ISR_STATUS      0x13

# define VIRTIO_SETADDR(X) (sizeof(X) == 32 ? (uint64)0 + (X) : (uint64)(X))

/************************************/
/*            Virtual Queue         */
/************************************/

typedef union {
  uint32 a32;
  uint64 a64;
} virtio_addr_u;

typedef struct {
    uint64 address;
    uint32 size;
    uint16 flags;
    uint16 next;
} __attribute__((packed)) t_virtio_vq_buffer;

typedef struct {
    uint16 flags;
    uint16 index;
    uint16 ring[0];
} __attribute__((packed)) t_virtio_vq_available;

typedef struct {
  uint32 index;
  uint32 length;
} t_virtio_vq_used_elt;

typedef struct {
    uint16 flags;
    uint16 index;
    t_virtio_vq_used_elt ring[];
} t_virtio_vq_used;

typedef struct {
    uint32 index;
    uint32 queue_index;
    t_virtio_vq_buffer    *buf;
    t_virtio_vq_available *available;
    t_virtio_vq_used      *used;
} __attribute__((packed)) t_virtio_vq_ring;

struct s_virtio_device;
typedef struct s_virtio_ops {
  void (*set_status)(struct s_virtio_device*, uint16);
  void (*set_isr_status)(struct s_virtio_device*, uint16);
} t_virtio_ops;

typedef struct s_virtio_device {
    struct pci_device        *dev;
    struct pci_config_device conf;
    t_virtio_vq_ring         rings[2];
    uint32                   ioaddr;
    t_virtio_ops             ops;
} t_virtio_device;


void driver_virtio_notify(t_virtio_device *dev, t_virtio_vq_ring *vq);
void driver_virtio_set_status(t_virtio_device *dev, uint16 status);

void driver_virtio_vq(t_virtio_device *dev, uint16 queue_index);
void driver_virtio_init(void);

void driver_virtio_send_buf(char *, uint32, t_virtio_device *, uint32);
void driver_virtio_get_buf(char *, uint32, t_virtio_device *, uint32);

vfs_ops *virtio_serial_get_ops(void);

#endif /* !VIRTIO_H_ */
