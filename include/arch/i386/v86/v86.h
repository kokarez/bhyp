#ifndef V86_H_
# define V86_H_

typedef struct {
  uint16 ax;
  uint16 bx;
  uint16 cx;
  uint16 dx;
} v86_request;

void v86_init(void);
void v86_int(uint16 num, v86_request *req);

#endif /* !V86_H_ */
