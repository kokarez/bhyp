#ifndef FRAMEBUFFER_H_
# define FRAMEBUFFER_H_

# include <arch/i386/asm/io.h>
# include <string.h>
# include <vfs/vfs.h>
# include <core/console.h>
# include <core.h>

# define COL 80
# define LIN 25
# define TAB 8

int fb_write(const char *str, uint len);
vfs_ops *fb_get_ops(void);
void fb_init(void);
void fb_clean(void);

#endif /* !FRAMEBUFFER_H_ */
