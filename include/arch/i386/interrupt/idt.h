#ifndef IDT_H_
# define	IDT_H_

# include <core.h>
# include <stdio.h>
# include <interrupt.h>

# define IDT_SIZE 0xFF
# define INT_GATE 0x8E00
# define TRAP_GATE 0xEF00
# define IDT_BASE 0x00000000

# define USER_MODE 3
# define SU_MODE   0

struct idt_descriptor
{
    uint16 offset_begin;
    uint16 selector;
    uint8 empty;
    uint8 type : 5;
    uint8 dpl : 2;
    uint8 p : 1;
    uint16 offset_end;
}__attribute__((packed));

struct idt_r
{
    uint16 limit;
    uint32 base;
}__attribute__((packed));

void int_0(void);

void pic_init(void);
void pic_irq_ack(int irq_nb);
void pic_irq_enable(int irq_nb);
void pic_irq_enable(int irq_nb);

void idt_init(void);
void idt_err_init(void);
void idt_handler(struct info_registers *regs);

void pit_init(void);

extern interrupt_handler idt_handlers[IDT_SIZE];

#endif	/* IDT_H_ */

