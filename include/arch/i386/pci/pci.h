#ifndef PCI_H_
# define PCI_H_

# define PCI_BAR_ADDR(bar) (bar & (~(0x03)))

struct pci_config_device {
  /* 00 */
  uint16 vendor_id;
  uint16 device_id;
  
  /* 04 */
  uint16 command;
  uint16 status;
  
  /* 08 */
  byte revision_id;
  byte prog_if;
  byte subclass;
  byte class_code;
  
  /* 0C */
  byte cache_line_size;
  byte latency_timer;
  byte header_type;
  byte bist;

  /* 10 */
  uint bar0;
  
  /* 14 */
  uint bar1;
  
  /* 18 */
  uint bar2;
  
  /* 1C */
  uint bar3;
  
  /* 20 */
  uint bar4;
  
  /* 24 */
  uint bar5;
  
  /* 28 */
  uint cardbus_cis_pointer;
  
  /* 2C */
  uint16 subsystem_vendo_id;
  uint16 subsystem_id;
  //uint io_base_addr_0;
  
  /* 30 */
  uint cardbuscis;
  
  /* 34 */
  byte capabilities_pointer;
  uint reserved_0 : 24;
  
  /* 38 */
  uint reserved_1;

  /* 3C */
  byte interrupt_line;
  byte interrupt_pin;
  byte min_grant;
  byte max_latency;

  /* 40 */
  uint16 subsystem_device_id;
  uint16 subsystem_vendor_id;
  
  /* 46 */
  uint pccard_16b;
} __attribute__((packed));

struct header_type_register {
  uint header_type : 7;
  uint mf : 1;
} __attribute__((packed));

enum pci_header_type {
  PCI_HEADER_TYPE_STANDARD = 0x0,
  PCI_HEADER_TYPE_PCI_2_PCI = 0x1,
  PCI_HEADER_TYPE_CARDBUS = 0x2,
};

struct pci_device {
  uint bus;
  uint dev;
  uint func;
};

typedef int (*pci_iter_func)(struct pci_device*, void*);

uint8  pci_config_read_byte(struct pci_device *dev, byte offset);
uint16 pci_config_read_word(struct pci_device *dev, byte offset);
uint32 pci_config_read_long(struct pci_device *dev, byte offset);

void pci_get_config(struct pci_device*, struct pci_config_device*);
int  pci_iter_bus(pci_iter_func f, void *param);
void pci_dump_config(void);

#endif /* !PCI_H_ */
