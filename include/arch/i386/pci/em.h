#ifndef EM_H_
# define EM_H_

# include <arch/i386/pci/pci_drivers.h>
# include <arch/i386/pci/pci.h>

  struct em_device {
    struct pci_device *dev;
    uint mmio_base;
    uint irq;
  };

#endif /* !EM_H_ */