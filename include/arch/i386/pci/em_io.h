#ifndef EM_IO_H_
# define EM_IO_H_

/* see six on git.lse.epita.fr */

# define EM_REG_CTRL     0x0000
# define EM_REG_STATUS   0x0008
# define EM_REG_EERD     0x0014

# define EM_REG_RAL      0x5400
# define EM_REG_RAH      0x5404

# define EM_REG_MTA      0x5200
# define EM_REG_MTA_NUM  127
# define EM_REG_MTA_SIZE 4

# define EM_REG_ICR	0x00C0
# define EM_REG_ITR      0x00C8
# define EM_REG_IMS      0x00D0
# define EM_REG_IMC      0x00D8

# define EM_REG_PBA	0x1000

# define EM_REG_RCTL     0x0100

# define EM_REG_RDAL     0x2800
# define EM_REG_RDAH     0x2804
# define EM_REG_RDLEN    0x2808
# define EM_REG_RDH      0x2810
# define EM_REG_RDT      0x2818
# define EM_REG_RDTR     0x2820

# define EM_REG_RADV     0x282C
# define EM_REG_RSRPD    0x2C00

# define EM_GET_REG(Base, Offset)        ((uint*)((uint)(Base) + (Offset)))

# define EM_IMS_TXDW     1 << 0
# define EM_IMS_TXQE     1 << 1
# define EM_IMS_LSC      1 << 2
# define EM_IMS_RXSEQ    1 << 3
# define EM_IMS_RXDMT0   1 << 4
/* reserved */
# define EM_IMS_RXO      1 << 6
# define EM_IMS_RXT0     1 << 7
/* reserved */
# define EM_IMS_MDAC     1 << 9
# define EM_IMS_XCFG     1 << 10
/* reserved */
# define EM_IMS_PHYINT   1 << 12
# define EM_IMS_GPI_1    1 << 13
# define EM_IMS_GPI_2    1 << 14
# define EM_IMS_TXD_LOW  1 << 15
# define EM_IMS_SRPD     1 << 16

struct em_rctl_reg {
  uint reserved_0 : 1;
  uint EN : 1;
  uint SBP : 1;
  uint UPE : 1;
  uint MPE : 1;
  uint LBM : 2;
  uint RDMTS : 2;
  uint reserved_1 : 2;
  uint MO : 2;
  uint reserved_2 : 1;
  uint BAM : 1;
  uint BSIZE : 2;
  uint VFE : 1;
  uint CFIEN : 1;
  uint CFI : 1;
  uint reserved_3 : 1;
  uint DPF : 1;
  uint PMCF : 1;
  uint reserved_4 : 1;
  uint BSEX : 1;
  uint SECRC : 1;
  uint reserved_5 : 5;
} __attribute__((packed));

# define EM_RCTL_RDMTS_1_2       0
# define EM_RCTL_RDMTS_1_4       1
# define EM_RCTL_RDMTS_1_8       2
# define EM_RCTL_RDMTS_RESERVED  3

struct em_ctrl_reg {
  uint FD : 1;
  uint reserved_21 : 2;
  uint LRST : 1;
  uint reserved_4 : 1;
  uint ASDE : 1;
  uint SLU : 1;
  uint ILOS : 1;
  uint SPEED : 2;
  uint reserved_10 : 1;
  uint FRCSPD : 1;
  uint FRCDPLX : 1;
  uint reserved_17_13 : 5;
  uint SPD0_DATA : 1;
  uint SPD1_DATA : 1;
  uint ADVD3WUC : 1;
  uint EN_PHY_PWR_MGMT : 1;
  uint SPD0_IODIR : 1;
  uint SPD1_IODIR : 1;
  uint reserved_25_24 : 2;
  uint RST : 1;
  uint RFCE : 1;
  uint TFCE : 1;
  uint reserved_29 : 1;
  uint VME : 1;
  uint PHY_RST : 1;
} __attribute__((packed));

struct em_status_reg {
  uint status : 14;
  uint FD : 1;
  uint LU : 1;
  uint function_ID : 2;
  uint TXOFF : 1;
  uint TBIMODE : 1;
  uint SPEED : 2;
  uint reserved : 1;
  uint PCI66 : 1;
  uint BUS64 : 1;
  uint PCIX_MODE : 1;
  uint PCIXSPD : 2;
  uint reserved_2 : 18;
} __attribute__((packed));

struct em_rdesc {
  uint64 address;
  uint16 length;
  uint16 checksum;
  uint8  status;
  uint8  errors;
  uint16 special;
} __attribute__((packed));

# define EM_RDESC_STATUS_DD      1 << 0
# define EM_RDESC_STATUS_EOP     1 << 1
# define EM_RDESC_STATUS_IXSM    1 << 2
# define EM_RDESC_STATUS_VP      1 << 3
# define EM_RDESC_STATUS_RSV     1 << 4
# define EM_RDESC_STATUS_TCPCS   1 << 5
# define EM_RDESC_STATUS_IPCS    1 << 6
# define EM_RDESC_STATUS_PIF     1 << 7

# define EM_RDESC_ERROR_CE       1 << 0
# define EM_RDESC_ERROR_SE_RSV   1 << 1
# define EM_RDESC_ERROR_SEQ_RSV  1 << 2
# define EM_RDESC_ERROR_RSV      1 << 3
# define EM_RDESC_ERROR_RSV_CXE  1 << 4
# define EM_RDESC_ERROR_TCPE     1 << 5
# define EM_RDESC_ERROR_IPE      1 << 6
# define EM_RDESC_ERROR_RXE      1 << 7

/* Statistic registers */

# define EM_REG_STATS_CRCERRS  0x4000
# define EM_REG_STATS_ALGNERRC 0x4004
# define EM_REG_STATS_SYMERRS  0x4008
# define EM_REG_STATS_RXERRC   0x400C
# define EM_REG_STATS_MPC      0x4010
# define EM_REG_STATS_SCC      0x4014
# define EM_REG_STATS_ECOL     0x4018
# define EM_REG_STATS_MCC      0x401C
# define EM_REG_STATS_LATECOL  0x4020
# define EM_REG_STATS_COLC     0x4028
# define EM_REG_STATS_DC       0x4030
# define EM_REG_STATS_TNCRS    0x4034
# define EM_REG_STATS_SEC      0x4038
# define EM_REG_STATS_CEXTERR  0x403C
# define EM_REG_STATS_RLEC     0x4040
# define EM_REG_STATS_XONRXC   0x4048
# define EM_REG_STATS_XONTXC   0x404C
# define EM_REG_STATS_XOFFRXC  0x4050
# define EM_REG_STATS_XOFFTXC  0x4054
# define EM_REG_STATS_FCRUC    0x4058
# define EM_REG_STATS_PRC64    0x405C
# define EM_REG_STATS_PRC127   0x4060
# define EM_REG_STATS_PRC255   0x4064
# define EM_REG_STATS_PRC511   0x4068
# define EM_REG_STATS_PRC1023  0x406C
# define EM_REG_STATS_PRC1522  0x4070
# define EM_REG_STATS_GPRC     0x4074
# define EM_REG_STATS_BPRC     0x4078
# define EM_REG_STATS_MPRC     0x407C
# define EM_REG_STATS_GPTC     0x4080
# define EM_REG_STATS_GORCL    0x4088
# define EM_REG_STATS_GORCH    0x408C
# define EM_REG_STATS_GOTCL    0x4090
# define EM_REG_STATS_GOTCH    0x4094
# define EM_REG_STATS_RNBC     0x40A0
# define EM_REG_STATS_RUC      0x40A4
# define EM_REG_STATS_RFC      0x40A8
# define EM_REG_STATS_RJC      0x40B0
# define EM_REG_STATS_MGTPRC   0x40B4
# define EM_REG_STATS_MGTPDC   0x40B8
# define EM_REG_STATS_MGTPTC   0x40BC
# define EM_REG_STATS_TORL     0x40C0
# define EM_REG_STATS_TORH     0x40C4
# define EM_REG_STATS_TOTL     0x40C8
# define EM_REG_STATS_TOTH     0x40CC
# define EM_REG_STATS_TPR      0x40D0
# define EM_REG_STATS_TPT      0x40D4
# define EM_REG_STATS_PTC64    0x40D8
# define EM_REG_STATS_PTC127   0x40DC
# define EM_REG_STATS_PTC255   0x40E0
# define EM_REG_STATS_PTC511   0x40E4
# define EM_REG_STATS_PTC1023  0x40E8
# define EM_REG_STATS_PTC1522  0x40EC
# define EM_REG_STATS_MPTC     0x40F0
# define EM_REG_STATS_BPTC     0x40F4
# define EM_REG_STATS_TSCTC    0x40F8
# define EM_REG_STATS_TSCTFC   0x40FC

#endif /* !EM_IO_H_ */
