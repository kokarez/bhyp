#ifndef PCI_DRIVERS_H_
# define PCI_DRIVERS_H_

# include <arch/i386/pci/pci.h>
# include <arch/i386/pci/pci_drivers_table.h>

struct pci_match_id {
  uint16 vendor;
  uint16 product;
};

struct pci_driver {
  const char *name;
  struct pci_match_id *matchids;
  void (*attach)(struct pci_device*);
};

void pci_init(void);
void pci_register(struct pci_driver *driver);

#endif /* !PCI_DRIVERS_H_ */