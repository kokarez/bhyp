#ifndef CPU_H_
# define CPU_H_

# include <arch/i386/interrupt/idt.h>

static inline void enable_irq(void)
{
  __asm__ volatile ("sti\n\t");
}

static inline void disable_irq(void)
{
  __asm__ volatile ("cli\n\t");
}

static inline void __halt(void)
{
  __asm__ volatile ("hlt");
}

static inline struct idt_r sidt(void)
{
  struct idt_r ret;

  __asm__("sidt %0\n\t"
          : "=m" (ret)
          :
          : "memory");
  return ret;
}

static inline void lidt(struct idt_r idtr)
{
  __asm__("lidt %0\n\t"
          :
          : "m" (idtr)
          : "memory");
}

static inline void invlpg(uint32 vaddr)
{
  __asm__("invlpg (%0)\n\t"
          :
          : "r" (vaddr)
          : "memory");
}

#endif /* !IRQ_H_ */
