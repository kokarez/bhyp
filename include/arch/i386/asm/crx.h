#ifndef CRX_H_
# define	CRX_H_

# define CR0_PE		(1 << 0)
# define CR0_PG		0x80000000

static inline uint32 get_cr0 (void)
{
     uint32 ret;
     __asm__("movl %%cr0, %0\n\t"
         	: "=&r" (ret));
     return ret;
}

static inline uint32 get_cr2 (void)
{
     uint32 ret;
     __asm__("movl %%cr2, %0\n\t"
         	: "=&r" (ret));
     return ret;
}

static inline void set_cr0 (uint32 v)
{
   	__asm__("movl %0, %%cr0\n\t"
          : /* No output */
         	: "r" (v)
         	: "memory");
}

static inline void set_cr3 (uint32 v)
{
   	__asm__("movl %0, %%cr3\n\t"
          : /* No output */
         	: "r" (v)
         	: "memory");
}

#endif	/* CRX_H_ */

