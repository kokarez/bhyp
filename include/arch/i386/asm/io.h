#ifndef IO_H_
# define IO_H_

# define outb(Port, Value)                                      \
  __asm__ volatile ("outb %%al, %%dx" : : "a"(Value), "d"(Port))

# define inb(Port, Value)                                       \
  __asm__ volatile ("inb %%dx, %%al" : "=a"(Value) : "d"(Port))

# define outw(Port, Value)                                      \
  __asm__ volatile ("outw %%ax, %%dx" : : "a"(Value), "d"(Port))

# define inw(Port, Value)                                       \
  __asm__ volatile ("inw %%dx, %%ax" : "=a"(Value) : "d"(Port))

# define outl(Port, Value)                                      \
  __asm__ volatile ("outl %%eax, %%dx" : : "a"(Value), "d"(Port))

# define inl(Port, Value)                                       \
  __asm__ volatile ("inl %%dx, %%eax" : "=a"(Value) : "d"(Port))

#endif /* !IO_H_ */
