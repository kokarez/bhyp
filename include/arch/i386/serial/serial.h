#ifndef SERIAL_H_
# define SERIAL_H_

# include <vfs/vfs.h>
# include <core/console.h>
# include <arch/i386/asm/io.h>

/* Les differents ports */
#define SERIAL_COM_1		0x3f8
#define	SERIAL_COM_2            0x2f8
#define SERIAL_COM_3		0x3e8
#define SERIAL_COM_4		0x2e8

/* Les vitesses */
#define	SERIAL_BR9600		0x0C
#define	SERIAL_BR19200		0x06
#define	SERIAL_BR38400		0x03
#define	SERIAL_BR57600		0x02
#define	SERIAL_BR115200         0x01

#define SERIAL_8N1		0x03
#define	SERIAL_7N1		0x02
#define	SERIAL_8N2		0x07
#define	SERIAL_7N2		0x06

/* Le type de fifo */
#define SERIAL_FIFO_14		0xC7
#define SERIAL_FIFO_8		0x87
#define	SERIAL_FIFO_4		0x47
#define SERIAL_FIFO_1		0x07

vfs_ops *serial_get_ops(int com);
void serial_read(uint32 port, char* data, uint32 size);
void serial_write(uint32 port, const char* data, uint32 size);
void serial_setup(uint32 port, uint8 rate, uint8 type);

#endif /* SERIAL_H_ */
