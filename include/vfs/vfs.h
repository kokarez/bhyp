#ifndef VFS_H_
# define VFS_H_

# define FD_MAX 512

typedef int (*write_ops)(const char *buf, uint len);
typedef int (*read_ops)(char *buf, uint len);

typedef struct {
  write_ops write;
  read_ops read;
} vfs_ops;

vfs_ops *vfs_get_ops(int fd);
int fd_register(vfs_ops *ops);
int __fd_register(vfs_ops *ops, int fd);


#endif /* !VFS_H_ */
