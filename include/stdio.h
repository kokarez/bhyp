#ifndef STDIO_H_
# define STDIO_H_

# include <stdarg.h>
# include <vfs/vfs.h>

int write(int fd, void *buf, int len);
int printf(const char *fmt, ...);
int fprintf(int fd, const char *fmt, ...);
int vprintf(int fd, const char *fmt, va_list ap);

#endif /* !STDIO_H_ */
