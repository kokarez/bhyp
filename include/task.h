#ifndef TASK_H_
# define TASK_H_

# include <arch/arch/task.h>

enum task_state {
  TASK_RUNNING,   /* Running on a CPU */
  TASK_WAITING,   /* Waiting for CPU time. */
  TASK_SLEEPING,  /* Waiting for a data to be ready */
  TASK_IDLING,    /* Inside the idle loop. */
  TASK_ZOMBIE
};

struct task;
struct task {
  uint state;
  uint pid;

  struct task *next;
  struct regs *regs;
  // fds
};

void task_finish_switch(struct task* prev, struct task* next);
void task_new(struct task* t, void* start_ip, enum task_type type);
void task_init(void);

#endif /* !TASK_H_ */
