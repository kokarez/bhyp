#Config base var
ROOT				?= .
BUILDDIR		?= .
include			$(BUILDDIR)/config.mk
KERNEL			?= bhyp
TARGET			?= bhyp.iso
FLOPPY			?= bhyp.floppy
TFTP			?= /tmp/

# verbose ?
ifeq ($(VERBOSE), 1)
	MAKEFLAGS	=
else
	MAKEFLAGS	= --silent --no-print-directory
endif

#Parse systeme
OBJS				:=
SUBDIRS			:= src
include 		$(foreach subdir, $(SUBDIRS), $(ROOT)/$(subdir)/Makefile)
DEPS				:= $(OBJS:.o=.d)

CONSOLE     = TEXT

#Flags
CFLAGS			= -DCONSOLE_$(CONSOLE) -Iinclude -Wall -Wextra -std=c99
CFLAGSP			= -include $(ROOT)/include/pervasive.h
CFLAGS			+= -nostdinc -fno-builtin
CFLAGS			+= -fno-stack-protector -g3 -gdwarf-2 -m32
LDFLAGS			= -nostdlib -m32 -Wl,--build-id=none
QEMU_PARAM  = -device virtio-serial -chardev stdio,id=vcons -device virtserialport,chardev=vcons,name=myfuckingserialvirtuel
#QEMU_PARAM += -device virtio-serial -chardev socket,path=/tmp/virtio,server,nowait,id=vhcons -device virtserialport,chardev=vhcons,name=myfuckingserialvirtuel
QEMU_PARAM += -net nic,model=virtio

.SUFFIXES:	.c .o .h .S

.PHONY: clean distclean grub qemu floppy

#Rules
all: $(KERNEL)

version:
	echo -n "const char version[] = \"bhyp-$(ARCH)-`date +"%d-%m-%Y %Hh%Mmin"`\";" > src/version.c

floppy:
	cp tools/$(ARCH)/floppy $(FLOPPY)
	mcopy -i $(FLOPPY) $(KERNEL) ::/modules/kernel

el: $(KERNEL)
	@echo -e "IMG\t$(TARGET)"
	tools/$(ARCH)/make_image.sh $(TARGET) $(KERNEL)

disk: $(KERNEL)
	@echo -e "IMG\t$(TARGET)"
	make -C tools/$(ARCH)/ $(TARGET)


$(KERNEL): src/arch/$(ARCH)/link.ld version $(OBJS)
	@echo -e "LD\t$@"
	$(CC) -o $@ $(OBJS) $(LDFLAGS) -T $<

%.o: %.c
	@echo -e "CC\t$<"
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CFLAGSP) -MMD -c $< -o $@

%.o: %.S
	@echo -e "AS\t$<"
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -MMD -c $< -o $@

debug: $(KERNEL) floppy
	$(QEMU) -fda $(FLOPPY) $(QEMU_PARAM) -s -S&
	sleep 1
	gdb $(KERNEL)

pxe: $(KERNEL) boot/pxe/main.o
	gcc $(LDFLAGS) boot/pxe/main.o -o pxelinux.0 -T boot/pxe/linker.ld
	gcc $(LDFLAGS) boot/pxe/main.o -o pxelinux.0 -Ttext 0x7c00
	cp pxelinux.0 $(TFTP)

pxe-qemu: pxe
	qemu -net nic -net user -bootp http://127.0.0.1:6969/pxelinux.0 -cdrom ipxe.iso

pxe-debug: pxe	
	qemu -net nic -net user -bootp http://127.0.0.1:6969/pxelinux.0 -cdrom ipxe.iso -s -S&
	gcc $(LDFLAGS) boot/pxe/main.o -o /tmp/debugpxe.o -Ttext 0x7c00
	sleep 1
	gdb /tmp/debugpxe.o
	

qemu: $(KERNEL) floppy
	$(QEMU) -fda $(FLOPPY) $(QEMU_PARAM)

grub: $(KERNEL) el
	$(QEMU) -cdrom $(TARGET)

bochs: $(KERNEL) floppy
	echo 6 | bochs -q 'megs:256' 'boot:floppy' 'floppya: 1_44=$(FLOPPY), status=inserted' #"com1: enabled=1, mode=term dev=`tty`"

bochs-debug: $(KERNEL) floppy
	echo 6 | bochs-gdb  -q 'megs:256' 'boot:floppy' 'floppya: 1_44=$(FLOPPY), status=inserted' 'gdbstub: enabled=1, port=1234, text_base=0, data_base=0, bss_base=0' 'log: bochsout.txt'&
			sleep 1
			gdb $(KERNEL)

clean:
	@echo -e "RM\t$(OBJS)"
	@echo -e "RM\t$(DEPS)"
	rm -f pxelinux.0
	rm -f boot/pxe/main.o
	rm -f boot/pxe/main.d
	rm -f $(OBJS)
	rm -f $(DEPS)
	@make -C tools/$(ARCH)/ clean

distclean: clean
	@echo -e "RM\t$(KERNEL) $(TARGET) $(FLOPPY) config.mk"
	rm -f $(KERNEL) $(TARGET) $(FLOPPY) config.mk
	rm -f include/arch/arch

-include $(DEPS)
