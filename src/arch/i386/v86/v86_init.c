#include <arch/i386/v86/v86.h>
#include <arch/i386/interrupt/idt.h>
#include <interrupt.h>

static void v86_handler()
{
  printf("Willkommen in protected mode");
}

void v86_init()
{
  int_register(v86_handler, 0x81, WRAPPED_FLAG | USER_FLAG);
}
