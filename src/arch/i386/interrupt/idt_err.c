#include <interrupt.h>
#include <core.h>
#include <arch/i386/interrupt/idt.h>

static void double_fault(struct info_registers *regs)
{
  (void) regs;
  panic("Double Fault\n");
}

static void inv_tss(struct info_registers *regs)
{
  (void) regs;
  panic("Invalid TSS\n");
}

static void inv_seg(struct info_registers *regs)
{
  (void) regs;
  panic("Segment Not Present\n");
}

static void ss_fault(struct info_registers *regs)
{
  (void) regs;
  panic("Stack Segment Fault\n");
}

static void gp_fault(struct info_registers *regs)
{
  (void) regs;
  panic("General Protection Fault\n");
}

void idt_err_init(void)
{
  int_register(double_fault, 8, WRAPPED_FLAG);
  int_register(inv_tss, 10, WRAPPED_FLAG);
  int_register(inv_seg, 11, WRAPPED_FLAG);
  int_register(ss_fault, 12, WRAPPED_FLAG);
  int_register(gp_fault, 13, WRAPPED_FLAG);
}
