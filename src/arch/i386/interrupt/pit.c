#include <interrupt.h>
#include <schedule.h>
#include <arch/i386/asm/io.h>
#include <arch/i386/interrupt/idt.h>
#include <core.h>

#define PIT_CMD_PORT 0x43
#define PIT_CNT1_PORT 0x40

static const int pit_original_frequency = 1193182;

static void pit_handler(struct info_registers *regs)
{
  (void) regs;
  schedule();
}

void pit_init(void)
{
  print_ok("Initialisation du pit");
  uint16 count = pit_original_frequency / 100;
  outb(PIT_CMD_PORT, 0x34);
  outb(PIT_CNT1_PORT, count & 0x00ff);
  outb(PIT_CNT1_PORT, (count & 0xff00) >> 8);
  int_register(pit_handler, 0x20, WRAPPED_FLAG);
}
