#include <arch/i386/interrupt/idt.h>
#include <arch/i386/asm/cpu.h>

static struct idt_r           idt_r;
static struct idt_descriptor  idt_descritors[IDT_SIZE];
interrupt_handler             idt_handlers[IDT_SIZE];


void int_register(interrupt_handler handler, uint nb, enum interrupt_flag flag)
{
  int dpl = (flag & USER_FLAG) ? USER_MODE : SU_MODE;
  int type = (flag & TRAP_FLAG) ? 0x0F : 0x0E;
  idt_handlers[nb] = handler;
  void *p = (flag & WRAPPED_FLAG) ?  (void*)((int)int_0 + nb * 16) : (void*)handler;

  idt_descritors[nb].offset_begin = (uint32)p & 0xffff;
  idt_descritors[nb].offset_end = ((uint32)p & 0xffff0000) >> 16;
  idt_descritors[nb].selector = 8;
  idt_descritors[nb].empty = 0;
  idt_descritors[nb].type = type;
  idt_descritors[nb].dpl = dpl;
  idt_descritors[nb].p  = 1;

  if (nb >= 32 && nb < 32 + 16)
      pic_irq_enable(nb - 32);
}

void idt_handler(struct info_registers *regs)
{
    if (idt_handlers[regs->int_num])
        idt_handlers[regs->int_num](regs);
    if (regs->int_num >= 32 && regs->int_num < 32 + 16)
        pic_irq_ack(regs->int_num - 32);
}

static void default_handler(struct info_registers *reg)
{
  printf("Unknowed int: %d\n", reg->int_num);
  printf("ebx:%x\n", reg->ebx);
  printf("ecx:%x\n", reg->ecx);
  printf("edx:%x\n", reg->edx);
  printf("esi:%x\n", reg->esi);
  printf("edi:%x\n", reg->edi);
  printf("ebp:%x\n", reg->ebp);
  printf("ds:%x\n", reg->ds);
  printf("eax:%x\n", reg->eax);
  printf("int num:%x\n", reg->int_num);
  printf("err num:%x\n", reg->err_num);
  halt();
}


void idt_init(void)
{
  int i;
  struct idt_r tmp;

  print_ok("Initialisation de l'idt");

  for (i = 0; i < IDT_SIZE; i++)
    int_register(default_handler,
                 i,
                 WRAPPED_FLAG | USER_FLAG);

  idt_r.limit = IDT_SIZE * sizeof(struct idt_descriptor) - 1;
  idt_r.base = (uint32)&idt_descritors;
  lidt(idt_r);
  tmp = sidt();
  printf("sidt base:0x%x  limite:%d(%d)\n", tmp.base, (tmp.limit - 1) / 8, tmp.limit);
  printf("myidt base:0x%x  limite:%d(%d)\n", idt_r.base, (idt_r.limit - 1) / 8, idt_r.limit);
}
