#include <arch/i386/interrupt/idt.h>
#include <arch/i386/asm/io.h>
#include <core.h>

void pic_init(void)
{
  print_ok("Initialisation du pic");

  /* Initialisation de ICW1 */
  outb(0x20, 0x11);
  outb(0xA0, 0x11);

  /* Initialisation de ICW2 */
  outb(0x21, 0x20);       /* vecteur de depart = 32 */
  outb(0xA1, 0x70);       /* vecteur de depart = 96 */

  /* Initialisation de ICW3 */
  outb(0x21, 0x04);
  outb(0xA1, 0x02);

  /* Initialisation de ICW4 */
  outb(0x21, 0x01);
  outb(0xA1, 0x01);

  /* masquage des interruptions */
  outb(0x21, 0xFB);
  outb(0xA1, 0xFF);
}

void pic_irq_enable(int irq_nb)
{
  uint8 mask;
    
  if (irq_nb < 8)
  {
    inb(0x21, mask);
    outb(0x21, mask & ~(1 << irq_nb));
  }
  else
  {
    inb(0xA1, mask);
    outb(0xA1, mask & ~(1 << (irq_nb - 8)));
  }
}

void pic_irq_disable(int irq_nb)
{
  uint8 mask;
    
  if (irq_nb < 8)
  {
    inb(0x21, mask);
    outb(0x21, mask | (1 << irq_nb));
  }
  else
  {
    inb(0xA1, mask);
    outb(0xA1, mask | (1 << (irq_nb - 8)));
  }
}

void pic_irq_ack(int irq_nb)
{
  outb(0x20, 0x20);
  if (irq_nb > 7)
    outb(0xA0, 0x20);
}
