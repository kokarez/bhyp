#include <arch/i386/serial/serial.h>
#include <arch/i386/virtio/virtio.h>
#include <external/multiboot.h>
#include <arch/i386/console/framebuffer.h>
#include <arch/i386/mm/gdt.h>
#include <arch/i386/mm/mmu.h>
#include <arch/i386/interrupt/idt.h>
#include <arch/i386/asm/cpu.h>
#include <arch/i386/pci/pci_drivers.h>
#include <arch/i386/pci/pci.h>
#include <arch/i386/video/vga.h>

void main(void);
void v86_int(void);
void v86_realModeStart(void);

void arch_main(uint32 magic, multiboot_info_t *info)
{
  (void)magic;
  (void)info;


  fb_init();
  serial_setup(SERIAL_COM_1, SERIAL_BR57600, SERIAL_8N1);
#ifdef CONSOLE_TEXT
  console_set(fb_get_ops());
#elif CONSOLE_SERIAl
  console_set(serial_get_ops());
#elif CONSOLE_VIRTIO
  console_set(virtio_serial_get_ops());
#endif

  vga_init();
  gdt_init();
  idt_init();
  idt_err_init();

  paging_mode_init(info);

  pic_init();
  pit_init();
  enable_irq();

  driver_virtio_init();
  pci_init();
  pci_dump_config();


  main();
}
