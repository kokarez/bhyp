#include <arch/i386/mm/gdt.h>
#include <arch/i386/mm/tss.h>
#include <core.h>

static struct gdte gdt[GDT_MAX_SIZE];
static struct gdtr gdtr;
static struct tss  tss;

int set_gdt_gate(uint32 base, uint32 limit, uint8 access, uint8 granularity, int n)
{
	struct gdte* gdte = &gdt[n];

	gdte->limit_0_15 = 0xffff & limit;
	gdte->base_0_15 = 0xffff & base;
	gdte->base_16_23 = (0xff0000 & base) >> 16;
	gdte->access = access;
	gdte->p = 1;
	gdte->limit_16_19 = (0xf0000 & limit) >> 16;
	gdte->avl = 0;
	gdte->l = 0;
	gdte->granularity = granularity;
	gdte->base_24_31 = (~0xffff & base) >> 24;

	return 1;
}

void gdt_init(void)
{
  print_ok("Initialisation de la gdt");
  gdtr.limit = sizeof(struct gdte) * GDT_MAX_SIZE - 1;
	gdtr.base = (uint32)&gdt;

	memset(&gdt, 0, sizeof(struct gdte));
  memset(&tss, 0, sizeof(struct tss));

  tss.io_map_base_address = (uint16)&tss.io - (uint16)&tss;
  tss.end = 0xFF;
  tss.eflags = (1 << 17) + (3 << 12) + (1 << 14);
  tss.idt[16] = (1 << 1); // (1 << 1);

  set_gdt_gate(0, 0xfffff, CODE_SEGMENT(0), 3, KERNEL_CS);
	set_gdt_gate(0, 0xfffff, DATA_SEGMENT(0), 3, KERNEL_DS);	
  set_gdt_gate(USER_BASE_CODE, 0x100, CODE_SEGMENT(3), 3, USER_CS);
	set_gdt_gate(USER_BASE_DATA, 0x100, DATA_SEGMENT(3), 3, USER_DS);
	set_gdt_gate(&tss, sizeof(tss), TSS_SEGMENT(0), 2, KERNEL_TSS);

  lgdt(gdtr);

  set_cr0(get_cr0() | CR0_PE);

  __asm__ volatile ("jmp 1f; 1:");

	set_ds(KERNEL_DATA_SELECTOR);
	set_es(0x00);
	set_fs(0x00);
	set_gs(0x00);
	set_ss(KERNEL_DATA_SELECTOR);
	set_cs(KERNEL_CODE_SELECTOR);

  ltrw(KERNEL_TSS_SELECTOR);
}

void set_tss_esp0(uint32 esp0)
{
  tss.ss0 = KERNEL_DATA_SELECTOR;
  tss.esp0 = esp0;
}
