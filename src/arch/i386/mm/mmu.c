#include <arch/i386/asm/crx.h>
#include <arch/i386/asm/cpu.h>
#include <arch/i386/mm/mmu.h>
#include <core.h>
#include <interrupt.h>
#include <stdio.h>
#include <string.h>

static mm_pde_t kernel_pd[1024]  __attribute__((aligned(4096)));
static mm_pte_t kernel_pt[1024][1024]  __attribute__((aligned(4096)));
static uint32 mmap_addr;
static uint32 mmap_len;

void *kmmap(uint size)
{
	mm_pte_t	*pte = kernel_pt[0];
	int		pg = 0;
	int		i = 0;
	int		nb_pages;
	uint32		res;

	if (size % MM_PG_SIZE == 0)
		nb_pages = size / MM_PG_SIZE;
	else
		nb_pages = (size / MM_PG_SIZE) + 1;

	while (i < nb_pages && pg < PT_SIZE * 1023)
	{
		if (pte->p)
			i = 0;
		else
			i++;

		pte++;
		pg++;
	}

	if (i < nb_pages)
		panic("No more pages left");

	for (i = 0; i < nb_pages; i++)
	{
		pte--;
    pg--;
		pte->p = 1;
	}

	res = ((pg / 1024) << 22) | ((pg % 1024) << 12);
#if 0
  printf("kmmap page:%d pd:%d pt:%d res:0x%x\n",
         nb_pages,
         pg / 1024,
         pg % 1024,
         (void*)res);
#endif
	return (void*)res;
}

int kmunmap(void *addr, uint lenght)
{
	int		pages;
  uint32 pdi, pti;

	if (((uint32)addr % MM_PG_SIZE) != 0)
		return -1;

	if ((lenght % MM_PG_SIZE) == 0)
		pages = lenght / MM_PG_SIZE;
	else
		pages = lenght / MM_PG_SIZE + 1;

	while (pages > 0)
	{
    pdi = (uint32)addr >> 22;
    pti = ((uint32)addr >> 12) & 0x3FF;
    kernel_pt[pdi][pti].p = 0;
    addr += MM_PG_SIZE;
    pages--;
	}

	return 0;
}


void page_fault_handler(struct info_registers *regs)
{
        int             lin_addr;
        pf_err_code_t   *pf_err;

	pf_err = (pf_err_code_t*)&regs->err_num;
	lin_addr = get_cr2();

	printf("Page Fault!\n");
	printf("Linear address: 0x%x\n", lin_addr);
	printf("\tPresent: %s\n", (pf_err->p ? "Yes" : "No"));
	printf("\tAccess causing fault: %s\n", (pf_err->wr ? "write" : "read"));
	printf("\tCaused by: %s\n", (pf_err->us ? "user" : "system"));
	if (pf_err->id)
		printf("\tCaused by fetched instruction\n");

	while (1)
		;
}




void paging_mode_init (multiboot_info_t *info)
{
	
	mm_cr3_t	cr3;
	int		i;

	mmap_addr = info->mmap_addr;
	mmap_len = info->mmap_length;
	memset(kernel_pd, 0, PD_SIZE);
	memset(kernel_pt, 0, 1024 * PT_SIZE);

	memset(&cr3, 0, sizeof(uint32));
	cr3.pde_addr = ((uint32)kernel_pd >> 12);
	cr3.pwt = 0;
	cr3.pcd = 0;


	for (i = 0; i < 1024; i++)
	{
		kernel_pd[i].p = 1;
		kernel_pd[i].rw = 1;
		kernel_pd[i].us = 1;
		kernel_pd[i].pte_addr = (uint32)(kernel_pt[i]) >> 12;
	}


	for (i = 0; i < 1024 * 1023; i++)
	{
		kernel_pt[0][i].p = 0;
		kernel_pt[0][i].rw = 1;
		kernel_pt[0][i].us = 1;
		kernel_pt[0][i].pf_addr = i;
	}

        kmmap(4096 * 1024 * 2);

	kernel_pd[1023].p = 1;
        kernel_pd[1023].rw = 1;
	kernel_pd[1023].us = 1;
	kernel_pd[1023].pte_addr = ((uint32)kernel_pd >> 12);

	set_cr3(*((uint32*)&cr3));

	int_register(page_fault_handler, 14, WRAPPED_FLAG);
	
	set_cr0(get_cr0() | CR0_PG);

	print_ok("Initialisation de la pagination");
}

