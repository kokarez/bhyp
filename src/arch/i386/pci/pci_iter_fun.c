#include <arch/i386/pci/pci_drivers.h>
#include <stdio.h>
#include <external/pcidevs.h>

extern struct pci_driver em_driver;

struct pci_driver *pci_drivers[126] = {0};

static struct {
  uint vendor_id;
  char *name;
} infos_vendor_str[] = {
  {PCI_VENDOR_INTEL, "Intel"},
  {PCI_VENDOR_AMD, "Amd"},
  {PCI_VENDOR_APPLE, "Apple"},
  {PCI_VENDOR_INNOTEK, "Innotek"},
  {PCI_VENDOR_QUMRANET, "Qumranet (~~ virtio)"},
  {0xFFFFFFFF, "N/A"},
  {0, 0}
};

/*
** Method to attach automaticly pci device
**
*/

int pci_match(struct pci_driver *dev, struct pci_match_id *match)
{
  for (uint i = 0; dev->matchids[i].vendor; ++i)
    if (dev->matchids[i].vendor == match->vendor
        && (dev->matchids[i].product == match->product))
      return 1;
  return 0;
}

int pci_attach_device(struct pci_device *dev, void *param)
{
  (void)param;

  uint vendor_id = pci_config_read_word(dev, 0);
  uint product_id = pci_config_read_word(dev, 2);

  struct pci_match_id match = { vendor_id, product_id };

  /* check list of knowned driver */
  for (uint i = 0; pci_drivers[i]; i++)
    if (pci_match(pci_drivers[i], &match) == 1) {
      printf("[!] pci attach => \"%s\" %x:%x\n",
             pci_drivers[i]->name,
             vendor_id,
             product_id);
      pci_drivers[i]->attach(dev);
      break;
    }
  return 0;
}

void pci_init(void)
{
  pci_iter_bus(pci_attach_device, NULL);
}

/*
** Method to debug pci
**
*/

int pci_device_dump(struct pci_device *dev, void* param)
{
  (void)param;

  uint vendor_id = pci_config_read_word(dev, 0);
  uint device_id = pci_config_read_word(dev, 2);
  char *name = NULL;

  for (uint i = 0; infos_vendor_str[i].vendor_id; i++)
  {
    name = infos_vendor_str[i].name;
    if (infos_vendor_str[i].vendor_id == vendor_id) break;
  }

  printf("%d:%d:%d => %x:%x => %s\n",
         dev->bus,
         dev->dev,
         dev->func,
         vendor_id,
         device_id,
         name);

  return 0;
}

void pci_dump_config(void)
{
  pci_iter_bus(pci_device_dump, NULL);
}

void pci_register(struct pci_driver *driver)
{
  struct pci_driver **tmp = pci_drivers;

  while (*tmp)
      tmp++;
  *tmp = driver;
}
