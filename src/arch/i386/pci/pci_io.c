#include <arch/i386/asm/io.h>
#include <arch/i386/pci/pci.h>

#define PCI_CONFIG_ADDRESS     0xcf8
#define PCI_CONFIG_DATA        0xcfc

struct pci_config_address {
  uint zero : 2;
  uint reg : 6;
  uint function : 3;
  uint device : 5;
  uint bus : 8;
  uint reserved : 7;
  uint enable : 1;
} __attribute__((packed));

uint32 pci_config_read_long(struct pci_device *dev, byte reg)
{
  uint32 res;

  struct pci_config_address address = {
    .zero = 0,
    .reg = reg,
    .function = dev->func,
    .device = dev->dev,
    .bus = dev->bus,
    .reserved = 0,
    .enable = 1
  };

  /*printf("");  XXX: why I need to do that ? */

  outl(PCI_CONFIG_ADDRESS, address);
  inl(PCI_CONFIG_DATA, res);
  
  return res;

}

uint8 pci_config_read_byte(struct pci_device *dev, byte offset)
{
  uint32 line = pci_config_read_long(dev, offset & 0xfc);

  return line >> ((offset & 2) * 8) & 0xff;
}

uint16 pci_config_read_word(struct pci_device *dev, byte offset)
{
  uint32 line = pci_config_read_long(dev, offset & 0xfc);

  return line >> ((offset & 2) * 8) & 0xffff;
}

void pci_get_config(struct pci_device *dev, struct pci_config_device *conf)
{
  /* FIXME */
  for (uint i = 0; i < 18; ++i)
    ((uint32*)conf)[i] = pci_config_read_long(dev, i);
}