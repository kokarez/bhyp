#include <arch/i386/pci/pci.h>

int pci_is_present(struct pci_device *device)
{
  return pci_config_read_long(device, 0) != ~((uint32)0);
}

int pci_iter_bus(pci_iter_func f, void *param)
{
  struct pci_device device = { 0, 0, 0 };

  for (device.bus = 0; device.bus < (1 << 8); ++device.bus)
  {
    for (device.dev = 0; device.dev < (1 << 5); ++device.dev)
    {
      for (device.func = 0; device.func < (1 << 3); ++device.func)
      {
        if (!pci_is_present(&device))
          continue;

        int res = f(&device, param);

        if (res != 0)
          return res;
      }
    }
  }

  return 0;
}