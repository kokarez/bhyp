#include <arch/i386/pci/em.h>
#include <arch/i386/pci/em_io.h>
#include <arch/i386/interrupt/idt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <external/pcidevs.h>
#include <interrupt.h>

static byte mac[] = { 0x52, 0x54, 0x11, 0x22, 0x33, 0x44 };

struct pci_match_id em_matchids[] = {
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_80003ES2LAN_CPR_DPT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_80003ES2LAN_SDS_DPT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_80003ES2LAN_CPR_SPT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_80003ES2LAN_SDS_SPT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82540EM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82540EM_LOM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82540EP },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82540EP_LOM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82540EP_LP },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82541EI },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82541EI_MOBILE },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82541ER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82541ER_LOM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82541GI },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82541GI_LF },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82541GI_MOBILE },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82542 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82543GC_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82543GC_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82544EI_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82544EI_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82544GC_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82544GC_LOM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82545EM_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82545EM_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82545GM_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82545GM_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82545GM_SERDES },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546EB_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546EB_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546EB_QUAD_CPR },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546GB_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546GB_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546GB_PCIE },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546GB_QUAD_CPR },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546GB_QUAD_CPR_K },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546GB_SERDES },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82546GB_2 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82547EI },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82547EI_MOBILE },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82547GI },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_AF },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_AT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_QUAD_CPR },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_QUAD_CPR_LP },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_QUAD_FBR },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_SERDES },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_SDS_DUAL },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571EB_SDS_QUAD },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82571PT_QUAD_CPR },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82572EI_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82572EI_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82572EI_SERDES },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82572EI },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82573E },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82573E_IAMT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82573E_PM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82573L },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82573L_PL_1 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82573L_PL_2 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82573V_PM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82574L },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82574LA },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82575EB_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82575EB_SERDES },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82575GB_QUAD_CPR },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82575GB_QP_PM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576_FIBER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576_SERDES },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576_QUAD_COPPER },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576_QUAD_CU_ET2 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576_NS },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576_NS_SERDES },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82576_SERDES_QUAD },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82577LC },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82577LM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82578DC },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82578DM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_82583V },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_82567V_3 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_IFE },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_IFE_G },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_IFE_GT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_IGP_AMT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_IGP_C },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_IGP_M },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH8_IGP_M_AMT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_BM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IFE },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IFE_G },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IFE_GT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IGP_AMT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IGP_C },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IGP_M },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IGP_M_AMT },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH9_IGP_M_V },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH10_D_BM_LF },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH10_D_BM_LM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH10_R_BM_LF },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH10_R_BM_LM },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_ICH10_R_BM_V },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_EP80579_LAN_1 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_EP80579_LAN_2 },
  { PCI_VENDOR_INTEL, PCI_PRODUCT_INTEL_EP80579_LAN_3 },
  { 0, 0 }
};

void em_attach(struct pci_device*);

struct pci_driver em_driver =
{
  "em",
  em_matchids,
  em_attach
};

struct em_device em_dev;

void em_set_addr(struct em_device *dev, byte *addr)
{
  /* Yeah, its ugly ! */
  uint tmp;
  tmp = addr[0] | addr[1] << 8 | addr[2] << 16 | addr[3] << 24;
  *EM_GET_REG(dev->mmio_base, EM_REG_RAL) = tmp;
  tmp = addr[4] | addr[5] << 8 | 0x80000000;
  *EM_GET_REG(dev->mmio_base, EM_REG_RAH) = tmp;
}

void em_int_handler(struct info_registers *regs)
{
  (void) regs;
  printf("Ethernet interrupt!\n");
}

void em_rx_buffer_init(struct em_device *dev, uint buffer_size)
{
  uint32 *addr = (uint32*)kalloc(4096);

  *EM_GET_REG(dev->mmio_base, EM_REG_RDAL) = (uint)addr;
  *EM_GET_REG(dev->mmio_base, EM_REG_RDAH) = 0;
  *EM_GET_REG(dev->mmio_base, EM_REG_RDLEN) = 4096;

  *EM_GET_REG(dev->mmio_base, EM_REG_RDH) = 0;
  *EM_GET_REG(dev->mmio_base, EM_REG_RDT) = 1;

  switch (buffer_size) {
#define SET_SIZE(Dev, BSize, BSex)            \
({                    \
  struct em_rctl_reg *rctl =              \
    (struct em_rctl_reg*)EM_GET_REG((Dev)->mmio_base, EM_REG_RCTL); \
  rctl->BSIZE = BSize;                \
  rctl->BSEX = BSex;                \
})

    case 256:   SET_SIZE(dev, 3, 0); break;
    case 512:   SET_SIZE(dev, 2, 0); break;
    case 1024:  SET_SIZE(dev, 1, 0); break;
    case 2048:  SET_SIZE(dev, 0, 0); break;
    case 4096:  SET_SIZE(dev, 3, 0); break;
    case 8192:  SET_SIZE(dev, 2, 0); break;
    case 16384: SET_SIZE(dev, 1, 0); break;

#undef SET_SIZE
    default:
      printf("%s : error invalid buffer size %d", __FUNCTION__, buffer_size);
      break;
  }

  ((struct em_rdesc*)addr)[0].address = (uint)kalloc(4096);
}

void em_reset(struct em_device *dev)
{
  int tmp = *EM_GET_REG(dev->mmio_base, EM_REG_ICR);
  (void)tmp;
  *EM_GET_REG(dev->mmio_base, EM_REG_ITR) = 1;

  *EM_GET_REG(dev->mmio_base, EM_REG_PBA) = 0x30;

  struct em_ctrl_reg ctrl;
  memset(&ctrl, 0, sizeof (ctrl));

  ctrl.LRST = 0;
  ctrl.SLU = 1;
  ctrl.ASDE = 1;
  ctrl.PHY_RST = 0;
  ctrl.VME = 0;
  ctrl.ILOS = 0;

  *EM_GET_REG(dev->mmio_base, EM_REG_CTRL) = *(uint*)&ctrl;

  /* Initialize Multicast Table Array */
  for (uint i = 0; i < EM_REG_MTA_NUM; ++i)
    *EM_GET_REG(dev->mmio_base, EM_REG_MTA + i * EM_REG_MTA_SIZE) = 0;

  uint32 *addr = (uint32*)kalloc(4096);

  *EM_GET_REG(dev->mmio_base, EM_REG_RDAL) = (uint)addr;
  *EM_GET_REG(dev->mmio_base, EM_REG_RDAH) = 0;
  *EM_GET_REG(dev->mmio_base, EM_REG_RDLEN) = 4096;

  *EM_GET_REG(dev->mmio_base, EM_REG_RDH) = 0;
  *EM_GET_REG(dev->mmio_base, EM_REG_RDT) = 0;

  struct em_rctl_reg rctl;
  memset(&rctl, 0, sizeof (rctl));

  rctl.EN = 1;
  rctl.SBP = 1;
  rctl.UPE = 1;
  rctl.MPE = 1;
  rctl.BAM = 1;
  rctl.SECRC = 1;

  *EM_GET_REG(dev->mmio_base, EM_REG_RCTL) = *(uint*)&rctl;

  *EM_GET_REG(dev->mmio_base, EM_REG_RDTR) = 0;
  *EM_GET_REG(dev->mmio_base, EM_REG_RADV) = 0;
  *EM_GET_REG(dev->mmio_base, EM_REG_RSRPD) = 0;

  em_rx_buffer_init(dev, 256);

  /* Disable all interrupts. */
  *EM_GET_REG(dev->mmio_base, EM_REG_IMC) = ~(uint)0;

  /* Enable interrupts. */
  *EM_GET_REG(dev->mmio_base, EM_REG_IMS) =
    (EM_IMS_RXT0 | EM_IMS_RXO | EM_IMS_RXDMT0 | EM_IMS_RXSEQ | EM_IMS_LSC);
}

void em_attach(struct pci_device *dev)
{

  em_dev.dev = dev;

  struct pci_config_device conf;

  pci_get_config(em_dev.dev, &conf);

  printf("vendor : %x\tdevice : %x\n", conf.vendor_id, conf.device_id);
  printf("command : %x\tstatus : %x\n", conf.command, conf.status);
  printf("\tbar0 : %x\n", conf.bar0);
  printf("\tbar1 : %x\n", conf.bar1);
  printf("\tirq : %x\n", conf.interrupt_line);
  printf("\tinterrupt_pin : %x\n", conf.interrupt_pin);

  em_dev.irq = conf.interrupt_line;

  int_register(em_int_handler, 0x20 + conf.interrupt_line, WRAPPED_FLAG);

  em_dev.mmio_base = (uint32)kalloc(32 * 4096);

  uint *ral = EM_GET_REG(em_dev.mmio_base, EM_REG_RAL);
  uint *rah = EM_GET_REG(em_dev.mmio_base, EM_REG_RAH);

  if (*ral == 0)
    em_set_addr(&em_dev, mac);

  printf("%x %x\n", *ral, *rah);
}
