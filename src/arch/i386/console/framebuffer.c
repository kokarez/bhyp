#include <arch/i386/console/framebuffer.h>

static unsigned int x_pos;
static unsigned int y_pos;
static unsigned char color = 0x7;
static char* fb = (char*)0xB8000;

vfs_ops i386_console_ops = {
  .write = fb_write
};

vfs_ops *fb_get_ops(void)
{
  return &i386_console_ops;
}

void fb_clean(void)
{
  int i = 0;

  for (; i < LIN * COL * 2; i++)
  {
    fb[i++] = ' ';
    fb[i] = 0x7;
  }
}

void fb_init(void)
{
  x_pos = 0;
  y_pos = 0;
  fb_clean();
}

static void cursor_update(void)
{
  unsigned int pos = y_pos * COL + x_pos;

  outb(0x3D4, 14);
  outb(0x3D5, (pos >> 8));
  outb(0x3D4, 15);
  outb(0x3D5, (pos & 0xFF));
}

static void scroll(void)
{
  int i = 0;

  memcpy(fb, fb + (COL * 2), COL * 24 * 2);
  for (; i < COL * 2; i++)
  {
    fb[COL * 24 * 2 + i++] = ' ';
    fb[COL * 24 * 2 + i] = 0x7;
  }
}

void kprint(const char* s, uint length)
{
  fb_write(s, length);
}

int fb_write(const char* s, uint length)
{
  int res = length;

  while (length--)
  {
    if (*s == '\n')
    {
      x_pos = 0;
      y_pos++;
      if (y_pos == 25)
      {
        scroll();
        y_pos -= 1;
      }
      s++;
      continue;
    }

    if (*s == '\t')
    {
      x_pos += TAB;
      x_pos -= x_pos % TAB;
      s++;
      continue;
    }

    if (*s == '\033')
    {
      s++;
      color = *s++;
      continue;
    }

    fb[(80 * y_pos + x_pos) * 2] = *s++;
    fb[(80 * y_pos + x_pos++) * 2 + 1] = color;
  }
  cursor_update();

  return res;
}
