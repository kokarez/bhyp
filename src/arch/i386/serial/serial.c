#include <arch/i386/serial/serial.h>

void serial_read(uint32 port, char* data, uint32 size)
{
  uint8 status;

  while (size)
  {
    inb(port + 5, status);

    if (status & 1)
    {
      inb(port, *data++);

      size--;
    }
  }
}

void serial_write(uint32 port, const char* data, uint32 size)
{
  uint8 status;

  while(size)
  {
    inb(port + 5, status);

    if (status & 0x20)
    {
      outb(port, *data++);
      size--;
    }
  }
}

static int serial_write_ops_com1(const char *buf, uint len)
{
    serial_write(SERIAL_COM_1, buf, len);
    return len;
}

static int serial_read_ops_com1(char *buf, uint len)
{
    serial_read(SERIAL_COM_1, buf, len);
    return len;
}

static vfs_ops i386_serail_ops_com1 = {
  .write = serial_write_ops_com1,
  .read = serial_read_ops_com1
};

vfs_ops *serial_get_ops(int com)
{
  if (com == SERIAL_COM_1)
    return &i386_serail_ops_com1;
  return NULL;
}

void serial_setup(uint32 port, uint8 rate, uint8 type)
{
  outb(port + 1, 0x00); 
  outb(port + 3, 0x80);
  outb(port + 0, rate);
  outb(port + 1, 0x00);
  outb(port + 3, type);
  outb(port + 2, SERIAL_FIFO_8);
  outb(port + 4, 0x08);
  
}
