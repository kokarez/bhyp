#include <arch/i386/virtio/virtio.h>
#include <stdio.h>

#if 0
static char* deviceid_type[] = {
    "Not defined",
    "Network Card",
    "Block Device",
    "Console",
    "Entropy Source",
    "Memory Ballooning",
    "IO Memory",
    "RPMSG",
    "SCSI Host",
    "9P Transport",
    "MAC802.11 WLAN"
};

static int driver_virtio_iter(struct pci_device *dev, void* param)
{
  (void)param;

  uint vendor_id = pci_config_read_word(dev, 0);
  uint device_id = pci_config_read_word(dev, 2);

  if (vendor_id == VIRTIO_VENDORID)
  {
      if (device_id < VIRTIO_DEVICEID_BEGIN ||
          device_id > VIRTIO_DEVICEID_END)
          printf("Bad virtio deviceid\n");

      if (device_id - VIRTIO_DEVICEID_BEGIN < 9)
          printf("Virtio device found (type:%s)\n", deviceid_type[device_id - VIRTIO_DEVICEID_BEGIN]);
  }

  return 0;
}
#endif

void driver_virtio_notify(t_virtio_device *dev, t_virtio_vq_ring *vq)
{
  outw(dev->ioaddr + VIRTIO_PCI_QUEUE_NOTIFY, vq->queue_index);
}

void driver_virtio_set_status(t_virtio_device *dev, uint16 status)
{
  uint16 res;

  inw(dev->ioaddr + VIRTIO_PCI_DEVICE_STATUS, res);
  outw(dev->ioaddr + VIRTIO_PCI_DEVICE_STATUS, res | status);
}

extern struct pci_driver virtio_console_driver;
extern struct pci_driver virtio_network_driver;

void driver_virtio_init()
{
    pci_register(&virtio_console_driver);
    pci_register(&virtio_network_driver);
}
