#include <arch/i386/virtio/virtio.h>
#include <arch/i386/asm/io.h>
#include <stdio.h>

void driver_virtio_vq(t_virtio_device *dev, uint16 queue_index)
{
    uint16 num, i;
    uint32 res32;

    outw(dev->ioaddr + VIRTIO_PCI_QUEUE_SEL, queue_index);
    inw(dev->ioaddr + VIRTIO_PCI_QUEUE_SIZE, num);
    if (num > 128)
    {
        printf("Bad value of num (%d)\n", num);
        return;
    }

    inl(dev->ioaddr + VIRTIO_PCI_QUEUE_ADDR, res32);
    if (res32)
    {
        printf("Queue already active (%d)\n", res32);
        return;
    }

    dev->rings[queue_index].queue_index = queue_index;
    dev->rings[queue_index].index = num;
    dev->rings[queue_index].buf = kmmap(4096);

    dev->rings[queue_index].available = (uint64)&dev->rings[queue_index].buf[dev->rings[queue_index].index];
    dev->rings[queue_index].available->flags |= VRING_AVAIL_F_NO_INTERRUPT;

    dev->rings[queue_index].used = (t_virtio_vq_buffer*)&dev->rings[queue_index].available->ring[dev->rings[queue_index].index];
    dev->rings[queue_index].used = (t_virtio_vq_buffer*)(((uint32)dev->rings[queue_index].used + 4095) & ~4095);

    for (i = 0; i < num - 1; i++)
      dev->rings[queue_index].buf[i].next = i + 1;
    dev->rings[queue_index].buf[i].next = 0;

    outl(dev->ioaddr + VIRTIO_PCI_QUEUE_ADDR, (uint32)dev->rings[queue_index].buf >> 12);

}

void driver_virtio_send_buf(char *buf, uint32 size, t_virtio_device *dev, uint32 queue_num)
{
    dev->rings[queue_num].buf[0].address = (uint64)buf;
    dev->rings[queue_num].buf[0].size = size;
    dev->rings[queue_num].available->index++;
    driver_virtio_notify(dev, dev->rings + queue_num);
}

void driver_virtio_get_buf(char *buf, uint32 size, t_virtio_device *dev, uint32 queue_num)
{
    dev->rings[queue_num].buf[0].address = (uint64)kmmap(size);
    dev->rings[queue_num].buf[0].size = size;
    dev->rings[queue_num].available->index++;
    driver_virtio_notify(dev, dev->rings + queue_num);

    /* wait for virtio device notify */
    *buf = NULL;
}
