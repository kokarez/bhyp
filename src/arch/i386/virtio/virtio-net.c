#include <arch/i386/console/framebuffer.h>
#include <arch/i386/virtio/virtio.h>
#include <stdio.h>

static uint8 device_count = 0;
static t_virtio_device virtio_devices[4];

static void driver_virtio_attach(struct pci_device* dev)
{

}

static struct pci_match_id driver_virtio_matchids[] = {
    { VIRTIO_VENDORID, VIRTIO_DEVICEID_BEGIN + 1 },
    { 0, 0}
};

struct pci_driver virtio_network_driver =
{
  "Virtio Network",
  driver_virtio_matchids,
  driver_virtio_attach
};