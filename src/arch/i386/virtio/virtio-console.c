#include <arch/i386/console/framebuffer.h>
#include <arch/i386/virtio/virtio.h>
#include <stdio.h>

static uint8 device_count = 0;
static t_virtio_device virtio_devices[4];

extern vfs_ops i386_console_ops;

static void driver_virtio_attach(struct pci_device* dev)
{
    uint8 id = device_count;

    virtio_devices[id].ops.set_status = driver_virtio_set_status;
    virtio_devices[id].dev = dev;

    pci_get_config(virtio_devices[id].dev, &virtio_devices[id].conf);

    virtio_devices[id].ioaddr = PCI_BAR_ADDR(virtio_devices[id].conf.bar0);

    virtio_devices[id].ops.set_status(virtio_devices + id, VIRTIO_CONFIG_S_ACKNOWLEDGE);
    virtio_devices[id].ops.set_status(virtio_devices + id, VIRTIO_CONFIG_S_DRIVER);

    driver_virtio_vq(virtio_devices + id, 4);
    driver_virtio_vq(virtio_devices + id, 5);

#if 0 /* crash qemu */
    driver_virtio_vq(virtio_devices + id, 2);
#endif

    virtio_devices[id].ops.set_status(virtio_devices + id, VIRTIO_CONFIG_S_DRIVER_OK);
    device_count++;
}

static struct pci_match_id driver_virtio_matchids[] = {
    { VIRTIO_VENDORID, VIRTIO_DEVICEID_BEGIN + 3 },
    { 0, 0}
};

struct pci_driver virtio_console_driver =
{
  "Virtio Console",
  driver_virtio_matchids,
  driver_virtio_attach
};

static int virtio_serial_write_com1(const char *buf, uint len)
{
  if (device_count == 0)
    return i386_console_ops.write(buf, len);
  driver_virtio_send_buf(buf, len, virtio_devices, 5);
  return len;
}

static int virtio_serial_read_com1(char *buf, uint len)
{
  /* FIXME */
  (void)buf;
  return len;
}


static vfs_ops virtio_serial_ops_com1 = {
  .write = virtio_serial_write_com1,
  .read = virtio_serial_read_com1
};

vfs_ops *virtio_serial_get_ops()
{
  return &virtio_serial_ops_com1;
}
