#include <core.h>
#include <string.h>
#include <stdio.h>

static void print_glue(const char *left, const char *rigth)
{
  uint llen = strlen(left);
  uint rlen = strlen(rigth);
  char line[80];

  llen = llen > 80 - rlen ? 80 - rlen : llen;

  for (uint i = 0; i < 80 - rlen; i++)
    if (i < llen)
      line[i] = left[i];
    else
      line[i] = ' ';

  line[80 - rlen] = '\0';
  printf("%s%s\n", line, rigth);
}

void print_center(const char *str)
{
  uint len = strlen(str);
  uint marging = 40 - (len / 2);
  char line[40];

  for (uint i = 0; i < marging; i++)
    line[i] = ' ';
  line[marging] = '\0';
  printf("%s%s\n", line, str);
}

void print_ok(const char *str)
{
  print_glue(str, "[ OK ]");
}

void print_ko(const char *str)
{
  print_glue(str, "[FAIL]");
}