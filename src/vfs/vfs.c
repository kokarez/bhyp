#include <vfs/vfs.h>

static vfs_ops *fds[FD_MAX] = {0};
static int current = 3;

vfs_ops *vfs_get_ops(int fd)
{
  return fds[fd];
}

int fd_register(vfs_ops *ops)
{
  return __fd_register(ops, -1);
}

int __fd_register(vfs_ops *ops, int fd)
{
  if (fd == -1)
    fd = current++;
  fds[fd] = ops;
  return fd;
}
