#include <core/console.h>


vfs_ops *console;

void console_set(vfs_ops *ops)
{
  console = ops;
  __fd_register(console, 0);
  __fd_register(console, 1);
  __fd_register(console, 2);
}
