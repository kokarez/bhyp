#include <core.h>
#include <string.h>

void panic(const char *str)
{
  kprint("===PANIC===\n", 12);
  kprint(str, strlen(str));
  kprint("===PANIC===\n", 12);

  while (1);
}
