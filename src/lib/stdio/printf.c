#include <stdio.h>

int printf(const char *fmt, ...)
{
  int res;
  va_list ap;

  va_start(ap, fmt);
  res = vprintf(1, fmt, ap);
  va_end(ap);

  return (res);
}

int fprintf(int fd, const char *fmt, ...)
{
  int res;
  va_list ap;

  va_start(ap, fmt);
  res = vprintf(fd, fmt, ap);
  va_end(ap);

  return (res);
}
