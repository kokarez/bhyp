#include <stdio.h>

int write(int fd, void *buf, int len)
{
  vfs_ops *ops = vfs_get_ops(fd);
  if (ops == NULL)
    return 0;
  return ops->write(buf, len);
}
