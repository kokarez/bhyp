#include <stdio.h>

typedef struct
{
    int   size;
    int   fd;
    char  buf[4096];
    int   write_size;
}         s_buff;

s_buff *get_my_buff(void)
{
  static s_buff my_buff = {0};

  return (&my_buff);
}

void print_char(char c)
{
  s_buff *my_buff;

  my_buff = get_my_buff();
  if (c == '\n')
  {
    my_buff->buf[my_buff->size++] = c;
    write(my_buff->fd, my_buff->buf, my_buff->size);
    my_buff->write_size += my_buff->size + 1;
    my_buff->size = 0;
    return;
  }
  if (my_buff->size >= 4095)
  {
    write(my_buff->fd, my_buff->buf, my_buff->size);
    my_buff->write_size += my_buff->size;
    my_buff->size = 0;
  }
  my_buff->buf[my_buff->size++] = c;
}

void print_unsigned_int(unsigned int ui)
{
  char c = '0';

  if (ui >= 10)
    print_unsigned_int(ui / 10);
  c += ui % 10;
  print_char(c);
}

void print_int(int i)
{
  if (i < 0)
  {
    i = 0 - i;
    print_char('-');
  }
  print_unsigned_int(i);
}

void print_string(char *s)
{
  if (!s)
  {
    print_char('(');
    print_char('n');
    print_char('u');
    print_char('l');
    print_char('l');
    print_char(')');
    return;
  }
  while (*s)
  {
    print_char(*s);
    s++;
  }
}

void print_octal(unsigned int i)
{
  char c = '0';

  if (i >= 8)
    print_octal(i / 8);
  c += i % 8;
  print_char(c);
}

void print_hexa(unsigned int i)
{
  char c = '0';

  if (i >= 16)
    print_hexa(i / 16);
  i = i % 16;
  if (i >= 10)
    print_char('a' + i - 10);
  else
    print_char(c + i);
}

int fflush_buffer(void)
{
  s_buff  *my_buff;
  int     size;

  my_buff = get_my_buff();
  if (my_buff->size)
    write(my_buff->fd, my_buff->buf, my_buff->size);
  my_buff->write_size += my_buff->size;
  my_buff->size = 0;
  size = my_buff->write_size;
  my_buff->write_size = 0;
  return (size);
}

void change_fd(int fd)
{
  s_buff  *buf;

  buf = get_my_buff();
  if (buf->fd == fd)
    return;
  fflush_buffer();
  buf->fd = fd;
}

int check_support(const char c)
{
  int res = 0;

  res = (c == 'd');
  res += (c == 's');
  res += (c == 'u');
  res += (c == 'c');
  res += (c == 'o');
  res += (c == 'x');
  return (res > 0);
}

int vprintf(int fd, const char *format, va_list vl)
{
  char c;

  change_fd(fd);
  while (*format)
  {
    if (*format == '%')
    {
      format++;
      c = *format;
      if (c == 'd')
        print_int(va_arg(vl, int));
      if (c == 's')
        print_string(va_arg(vl, char *));
      if (c == 'u')
        print_unsigned_int(va_arg(vl, unsigned int));
      if (c == 'c')
        print_char(va_arg(vl, int));
      if (c == 'o')
        print_octal(va_arg(vl, unsigned int));
      if (c == 'x')
        print_hexa(va_arg(vl, unsigned int));
      if (!check_support(c))
      {
        print_char('%');
        print_char(c);
      }
    }
    else
      print_char(*format);
    format++;
  }
  va_end(vl);
  return (fflush_buffer());
}
