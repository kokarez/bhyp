#include <string.h>

void *memcpy(void *dst, const void *src, uint n)
{
  for (uint i = 0; i < n; ++i)
    ((byte*) dst)[i] = ((byte*) src)[i];

  return (dst);
}

void *memset(void *s, int b, uint n)
{
  for (uint i = 0; i < n; ++i)
    ((byte*) s)[i] = b;

  return (s);
}
