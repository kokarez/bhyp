#include <string.h>

uint strlen(const char *s)
{
  const char *p = s;

  while (*p)
    ++p;

  return (p - s);
}
