#include <stdlib.h>

static uint pos = 0;

void *kalloc(uint size)
{
  void *ptr = (void*)pos;
  
  if (!pos)
    pos = _stack_start;
  pos += size;
  return ptr;
}

void kfree(void *ptr)
{
  (void)ptr;
}
